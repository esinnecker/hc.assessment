﻿using HC.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HC.Assessment.Infra.Data.ORM.EF
{
	public class AssessmentDbContext : DbContext
	{
		public AssessmentDbContext() : base("connHCDbContext")
		{
		}

		public DbSet<Shirts> Shirts { get; set; }
		public DbSet<Ties> Ties { get; set; }
		public DbSet<Cufflinks> Cufflinks { get; set; }
		public DbSet<Bags> Bags { get; set; }
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			//Removendo convencoes
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
			modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

			//E003 - Add FluentAPI Map
			//modelBuilder.Configurations.Add(new CountryMap());
			//modelBuilder.Configurations.Add(new CityMap());
			//modelBuilder.Configurations.Add(new MuralMap());
			//E003 - Add FluentAPI Map - END

			modelBuilder.Properties()
				.Where(p => p.Name == p.ReflectedType.Name + "Id")
				.Configure(p => p.IsKey());

			modelBuilder.Properties<string>()
				.Configure(p => p.HasColumnType("varchar"));

			modelBuilder.Properties<string>()
				.Configure(p => p.HasMaxLength(80));

			modelBuilder.Entity<Shirts>().Property(x => x.FilePath).HasMaxLength(250);
			modelBuilder.Entity<Ties>().Property(x => x.FilePath).HasMaxLength(250);
			modelBuilder.Entity<Cufflinks>().Property(x => x.FilePath).HasMaxLength(250);
			base.OnModelCreating(modelBuilder);
		}
	}
}
