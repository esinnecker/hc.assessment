﻿using HC.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HC.Assessment.Web.ViewModel
{
	public class ProductViewModel
	{
		public IEnumerable<Shirts> Shirts { get; set; }
		public IEnumerable<Ties> Ties { get; set; }
		public IEnumerable<Cufflinks> Cufflinks { get; set; }
	}
}