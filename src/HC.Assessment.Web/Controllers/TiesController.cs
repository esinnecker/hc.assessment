﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HC.Assessment.Infra.Data.ORM.EF;
using HC.Assessment.Domain.Entities;
using System.IO;

namespace HC.Assessment.Web.Controllers
{
    public class TiesController : Controller
    {
        private AssessmentDbContext db = new AssessmentDbContext();

        // GET: Ties
        public async Task<ActionResult> Index()
        {
            return View("Index", "_Layout", await db.Ties.ToListAsync());
            //return View(await db.Ties.ToListAsync());
        }

        // GET: Ties/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ties ties = await db.Ties.FindAsync(id);
            if (ties == null)
            {
                return HttpNotFound();
            }
            return PartialView(ties);
        }
        public ActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        // GET: Ties/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: Ties/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "Id,Description,Details,Price,FilePath")] Ties ties)
        public ActionResult Create(Ties ties, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                string path = Path.Combine(Server.MapPath("~/Images/Ties"), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                db.Ties.Add(new Ties
                {
                    Description = ties.Description,
                    Details = ties.Details,
                    Price = ties.Price,
                    FilePath = "Images/Ties/" + file.FileName
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ties);
        }

        // GET: Ties/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ties ties = await db.Ties.FindAsync(id);
            if (ties == null)
            {
                return HttpNotFound();
            }
            return PartialView(ties);
        }

        // POST: Ties/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Ties ties, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string path = Path.Combine(Server.MapPath("~/Images/Ties"), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ties.FilePath = "Images/Ties/" + file.FileName;
                }
                db.Entry(ties).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ties);
        }

        // GET: Ties/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ties ties = await db.Ties.FindAsync(id);
            if (ties == null)
            {
                return HttpNotFound();
            }
            return PartialView(ties);
        }

        // POST: Ties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Ties ties = await db.Ties.FindAsync(id);
            db.Ties.Remove(ties);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
