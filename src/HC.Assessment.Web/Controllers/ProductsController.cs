﻿using HC.Assessment.Infra.Data.ORM.EF;
using HC.Assessment.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HC.Assessment.Web.Controllers
{
    public class ProductsController : Controller
    {
        private AssessmentDbContext db = new AssessmentDbContext();
        // GET: Products
        public ActionResult Index()
        {
			//dynamic mymodel = new ExpandoObject();
			//mymodel.Shirts = db.Shirts.ToListAsync();
			//mymodel.Ties = db.Ties.ToListAsync();
			//mymodel.Cufflinks = db.Cufflinks.ToListAsync();
			//return View("Index", "_LayoutProduct", mymodel);
			////return View(mymodel);    

			ProductViewModel model = new ProductViewModel();
            model.Shirts = db.Shirts.ToList();
            model.Ties = db.Ties.ToList();
            model.Cufflinks = db.Cufflinks.ToList();
            return View("Index", "_LayoutProduct", model);
        }
    }
}