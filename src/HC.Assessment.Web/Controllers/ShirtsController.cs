﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HC.Assessment.Infra.Data.ORM.EF;
using HC.Assessment.Domain.Entities;
using System.IO;

namespace HC.Assessment.Web.Controllers
{
    public class ShirtsController : Controller
    {
        private AssessmentDbContext db = new AssessmentDbContext();

        // GET: Shirts
        public async Task<ActionResult> Index()
        {
            return View("Index", "_Layout", await db.Shirts.ToListAsync());
            //return View(await db.Shirts.ToListAsync());
        }

        // GET: Shirts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shirts shirts = await db.Shirts.FindAsync(id);
            if (shirts == null)
            {
                return HttpNotFound();
            }
            return PartialView(shirts);
        }
        public ActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        // GET: Shirts/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: Shirts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "Id,Description,Details,Price,FilePath")] Shirts shirts)
        public ActionResult Create(Shirts shirts, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                string path = Path.Combine(Server.MapPath("~/Images/Shirts"), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                db.Shirts.Add(new Shirts
                {
                    Description = shirts.Description,
                    Details = shirts.Details,
                    Price = shirts.Price,
                    FilePath = "Images/Shirts/" + file.FileName
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shirts);
        }

        // GET: Shirts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shirts shirts = await db.Shirts.FindAsync(id);
            if (shirts == null)
            {
                return HttpNotFound();
            }
            return PartialView(shirts);
        }

        // POST: Shirts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Shirts shirts, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string path = Path.Combine(Server.MapPath("~/Images/Shirts"), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    shirts.FilePath = "Images/Shirts/" + file.FileName;
                }
                db.Entry(shirts).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shirts);
        }

        // GET: Shirts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shirts shirts = await db.Shirts.FindAsync(id);
            if (shirts == null)
            {
                return HttpNotFound();
            }
            return PartialView(shirts);
        }

        // POST: Shirts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Shirts shirts = await db.Shirts.FindAsync(id);
            db.Shirts.Remove(shirts);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
