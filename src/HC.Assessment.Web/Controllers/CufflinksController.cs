﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HC.Assessment.Infra.Data.ORM.EF;
using HC.Assessment.Domain.Entities;
using System.IO;

namespace HC.Assessment.Web.Controllers
{
    public class CufflinksController : Controller
    {
        private AssessmentDbContext db = new AssessmentDbContext();

        // GET: Cufflinks
        public async Task<ActionResult> Index()
        {
            return View("Index", "_Layout", await db.Cufflinks.ToListAsync());
            //return View(await db.Cufflinks.ToListAsync());
        }

        // GET: Cufflinks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cufflinks cufflinks = await db.Cufflinks.FindAsync(id);
            if (cufflinks == null)
            {
                return HttpNotFound();
            }
            return PartialView(cufflinks);
        }
        public ActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        // GET: Cufflinks/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: Cufflinks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "Id,Description,Details,Price,FilePath")] Cufflinks cufflinks)
        public ActionResult Create(Cufflinks cufflinks, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                string path = Path.Combine(Server.MapPath("~/Images/Cufflinks"), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                db.Cufflinks.Add(new Cufflinks
                {
                    Description = cufflinks.Description,
                    Details = cufflinks.Details,
                    Price = cufflinks.Price,
                    FilePath = "Images/Cufflinks/" + file.FileName
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cufflinks);
        }

        // GET: Cufflinks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cufflinks cufflinks = await db.Cufflinks.FindAsync(id);
            if (cufflinks == null)
            {
                return HttpNotFound();
            }
            return PartialView(cufflinks);
        }

        // POST: Cufflinks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Cufflinks cufflinks, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string path = Path.Combine(Server.MapPath("~/Images/Cufflinks"), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    cufflinks.FilePath = "Images/Cufflinks/" + file.FileName;
                }
                db.Entry(cufflinks).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cufflinks);
        }

        // GET: Cufflinks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cufflinks cufflinks = await db.Cufflinks.FindAsync(id);
            if (cufflinks == null)
            {
                return HttpNotFound();
            }
            return PartialView(cufflinks);
        }

        // POST: Cufflinks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Cufflinks cufflinks = await db.Cufflinks.FindAsync(id);
            db.Cufflinks.Remove(cufflinks);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
