﻿using HC.Assessment.Domain.Entities;
using HC.Assessment.Infra.Data.ORM.EF;
using HC.Assessment.Web.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HC.Assessment.Web.Controllers
{
    public class BagsController : Controller
    {
        private AssessmentDbContext db = new AssessmentDbContext();

        // GET: Bags
        public ActionResult Index()
        {
            List<BagView> bagview = new List<BagView>();
            
            foreach (var bv in db.Bags.ToList())
			{
				switch (bv.Type)
				{
                    case "SHIRT":
                        var shirt = db.Shirts.Where(s => s.Id.Equals(bv.IdProduct)).FirstOrDefault();
                        bagview.Add(new BagView() { Id = bv.Id, IdProduct = bv.IdProduct, Description = shirt.Description, FilePath = shirt.FilePath, Price = shirt.Price });                     
                        break;
                    case "TIE":
                        var tie = db.Ties.Where(t => t.Id.Equals(bv.IdProduct)).FirstOrDefault();
                        bagview.Add(new BagView() { Id = bv.Id, IdProduct = bv.IdProduct, Description = tie.Description, FilePath = tie.FilePath, Price = tie.Price });
                        break;
                    case "CUFFLINK":
                        var cufflink = db.Cufflinks.Where(c => c.Id.Equals(bv.IdProduct)).FirstOrDefault();
                        bagview.Add(new BagView() { Id = bv.Id, IdProduct = bv.IdProduct, Description = cufflink.Description, FilePath = cufflink.FilePath, Price = cufflink.Price });
                        break;
				}				
			}
            return View("Index", "_Layout", bagview);
        }
        
        [HttpPost]
        public ActionResult SaveBag(int id, string type)
		{
            if (ModelState.IsValid)
            {
                db.Bags.Add(new Bags
                {
                    IdProduct = id,
                    Type = type
                });
                db.SaveChanges();

                return Json(new { success = true, message = "Product has added in the bag!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //IEnumerable<ModelError> errors = ModelState.Values.SelectMany(items => items.Errors).;

                //var errors = ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToList();

                var errors = string.Join("<br>", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));   // html break instead of " | "

                return Json(new { success = false, message = errors }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

		// GET: Shirts/Delete/5
		public async Task<ActionResult> Delete(int? id)
		{
			BagView bagview = new BagView();

			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Bags bags = await db.Bags.FindAsync(id);

			switch (bags.Type)
			{
				case "SHIRT":
					var shirt = db.Shirts.Where(s => s.Id.Equals(bags.IdProduct)).FirstOrDefault();
					bagview.Id = bags.Id;
                    bagview.Description = shirt.Description;
                    bagview.Price = shirt.Price;
                    bagview.FilePath = shirt.FilePath;
					break;
				case "TIE":
					var tie = db.Ties.Where(t => t.Id.Equals(bags.IdProduct)).FirstOrDefault();
                    bagview.Id = bags.Id;
                    bagview.Description = tie.Description;
                    bagview.Price = tie.Price;
                    bagview.FilePath = tie.FilePath;
                    break;
				case "CUFFLINK":
					var cufflink = db.Cufflinks.Where(c => c.Id.Equals(bags.IdProduct)).FirstOrDefault();
                    bagview.Id = bags.Id;
                    bagview.Description = cufflink.Description;
                    bagview.Price = cufflink.Price;
                    bagview.FilePath = cufflink.FilePath;
                    break;
			}

			if (bagview == null)
			{
				return HttpNotFound();
			}
			return PartialView(bagview);
		}

		// POST: Shirts/Delete/5
		[HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Bags bags = await db.Bags.FindAsync(id);
            db.Bags.Remove(bags);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}