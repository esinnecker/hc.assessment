﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HC.Assessment.Web.Entity
{
	public class BagView
	{
		public int Id { get; set; }
		public int IdProduct { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public string FilePath { get; set; }
	}
}