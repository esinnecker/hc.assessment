﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HC.Assessment.Domain.Entities
{
	public class Cufflinks
	{
		[Key]
		public int Id { get; set; }
		public string Description { get; set; }
		[StringLength(2000)]
		public string Details { get; set; }

		[Range(0, 100, ErrorMessage = "Please enter valid value")]
		public decimal Price { get; set; }
		public string FilePath { get; set; }
	}
}
