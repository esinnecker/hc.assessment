﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HC.Assessment.Domain.Entities
{
	public class Bags
	{
		[Key]
		public int Id { get; set; }
		public int IdProduct { get; set; }
		public string Type { get; set; }
	}
}
